package ch.quantasy.hyperdrive.emergency.mqtt;

import java.util.HashSet;
import java.util.Set;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

public class MqttHandler {

    private final MqttAsyncClient client;
    private final MqttConnectOptions options;
    private final Set<MQTTMessageListener> messageListeners;

    public MqttHandler(String broker, String clientId)throws MqttException{
        messageListeners = new HashSet<>();
        client = new MqttAsyncClient(broker, clientId, new MemoryPersistence());
        client.setCallback(new MessageHandler());
        options = new MqttConnectOptions();
        options.setAutomaticReconnect(true);
        options.setCleanSession(true);
    }
    
    public void connect(Will will)throws MqttException{
        options.setWill(will.topic(), will.last(),1,true);
        client.connect(options).waitForCompletion();
        client.publish(will.topic(), will.first(),1,true);
    }
    

    public void subscribe(String topic) throws MqttException {
        client.subscribe(topic, 1);  // QoS level 1
    }

    public void unsubscribe(String topic) throws MqttException {
        client.unsubscribe(topic);
    }

    public void publish(String topic, String payload, boolean retained) throws MqttException {
        MqttMessage message = new MqttMessage(payload.getBytes());
        message.setQos(1);
        message.setRetained(retained);
        client.publish(topic, message);
    }

    public void addMessageListener(MQTTMessageListener listener) {
        this.messageListeners.add(listener);
    }

    public void removeMessageListener(MQTTMessageListener listener) {
        this.messageListeners.remove(listener);
    }

    class MessageHandler implements MqttCallbackExtended {

        @Override
        public void messageArrived(String topic, MqttMessage message) throws Exception {
            for (MQTTMessageListener listener : messageListeners) {
                listener.messageArrived(topic, message);
            }
        }

        @Override
        public void connectComplete(boolean bln, String string) {
        }

        @Override
        public void connectionLost(Throwable thrwbl) {
        }

        @Override
        public void deliveryComplete(IMqttDeliveryToken imdt) {
        }
    }
}
