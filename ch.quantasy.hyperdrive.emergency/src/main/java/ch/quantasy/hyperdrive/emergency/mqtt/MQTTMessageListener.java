package ch.quantasy.hyperdrive.emergency.mqtt;

import org.eclipse.paho.client.mqttv3.MqttMessage;

public interface MQTTMessageListener {
    public void messageArrived(String topic, MqttMessage message);
    
}