package ch.quantasy.hyperdrive.emergency;

import ch.quantasy.hyperdrive.emergency.mqtt.Will;
import ch.quantasy.hyperdrive.emergency.mqtt.MqttHandler;
import java.io.IOException;
import org.eclipse.paho.client.mqttv3.MqttException;

public class Main {

    public static void main(String[] args) {
        try {
            MqttHandler mqttHandler = new MqttHandler("tcp://localhost:1883", "EmergencyProxyQuantasy");
            mqttHandler.connect(new Will("EmergencyProxy/U/quantasy/S/online","{\"value\":true}".getBytes(),"{\"value\":false}".getBytes()));
            EmergencyProxy emergencyProxy = new EmergencyProxy(mqttHandler);
            Thread emergencyProxyThread = new Thread(emergencyProxy);

            
            emergencyProxyThread.start();

            
            System.in.read();
            emergencyProxyThread.interrupt();
        } catch (IOException | MqttException e) {
            e.printStackTrace();
        }
    }
}
