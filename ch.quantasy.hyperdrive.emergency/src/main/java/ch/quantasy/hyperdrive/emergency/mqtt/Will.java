package ch.quantasy.hyperdrive.emergency.mqtt;

/**
 *
 * @author reto
 */
public record Will(String topic, byte[] first, byte[] last) {
    
}
